package sitemap

import (
	"crypto/md5"
	"fmt"
	"io/ioutil"
	"net"
	"net/http"
	"os"
	"regexp"
	"strings"
	"time"
	//"gopkg.in/redis.v2"
)

const (
	DEFAULT_USER_AGENT = "5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/42.0.2311.135 Safari/537.36"
)

type Configuration struct {
	WithSubdomains 	bool
	RequestTimeout	time.Duration
	Limit          	int
	UserAgent      	string
	Cookies			[]string
	Verbose			bool
}

type Sitemap struct {
	Configuration Configuration
	StartUrl      string

	// array width unique hrefs
	urls []string

	// array width other uniqu hrefs
	Other []string

	// statistics
	startTime time.Time
	
	totalRequests	uint32
	successRequests	uint32
	errorRequests	uint32
	
	ErrorList []string
	
	summary summary
}

type HttpResponse struct {
	url      string
	response *http.Response
	content  string
	err      error
}

func NewSitemap(url string, conf Configuration) *Sitemap {

	s := new(Sitemap)
	s.StartUrl = url

	if conf.UserAgent == "" {
		conf.UserAgent = DEFAULT_USER_AGENT
	}
	s.Configuration = conf

	return s
}

func (s *Sitemap) Run() {

	s.clearStats()
	s.getAll([]string{s.StartUrl})
	s.setSummary()
}

func (s *Sitemap) GetLinks() []string {
	
	links := []string{}
	
	for _, link := range s.urls {
		
		parsed, err := ParseUrl(link)
		if err != nil {
			dump(err)
			continue
		}
		links = append(links, parsed.path)
	}
	
	return links
}

func (s *Sitemap) clearStats() {

	s.startTime = time.Now()
	s.totalRequests = 0
	s.errorRequests = 0
	s.successRequests = 0
}

func (s *Sitemap) getAll(urls []string) {

	grabbed := s.getOne(urls)
	if len(grabbed) == 0 {
		return
	}

	new_urls := []string{}

	for _, new_url := range grabbed {

		if inArray(new_url, s.urls) == false {
			s.urls = append(s.urls, new_url)

			if len(s.urls) < s.Configuration.Limit {
				new_urls = append(new_urls, new_url)
			}
		}
	}
	if len(new_urls) > 0 {
		s.getAll(new_urls)
	}
}

func dialTimeout(network, addr string) (net.Conn, error) {

	return net.DialTimeout(network, addr, time.Duration(15*time.Second))
}

func clearUrl(url string) string {

	if url[len(url)-1:] == "/" {
		return url
	}
	return url
}

func (s *Sitemap) AsyncHttpGets(urls []string) []*HttpResponse {

	ch := make(chan *HttpResponse, len(urls)) // buffered
	responses := []*HttpResponse{}
	erroras := []string{}
	timing := make(map[[16]byte]time.Time)
/*
	redis_client := redis.NewTCPClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})
*/
	for _, url := range urls {
		go func(url string) {
			//fmt.Printf("Fetching %s \n", url)

			transport := http.Transport{
				Dial: dialTimeout,
			}
			client := http.Client{
				Transport: &transport,
			}

			timing[md5.Sum([]byte(url))] = time.Now()

			req, err := http.NewRequest("GET", url, nil)
			s.totalRequests ++
			if err != nil {
				dump("error NewRequest " + url)
				s.errorRequests ++
				s.ErrorList = append(s.ErrorList, err.Error())
				errorLog(err.Error())
				erroras = append(erroras, url)
				return
			}
			req.Header.Set("User-Agent", s.Configuration.UserAgent)
			req.Header.Set("Cookie", "mf_user=1") 
			
			resp, err := client.Do(req)
		
			if err != nil {
				dump("error client.Do" + url)
				s.errorRequests ++
				errorLog(err.Error())
				s.ErrorList = append(s.ErrorList, err.Error())
				erroras = append(erroras, url)
				return
			}

			content, err := ioutil.ReadAll(resp.Body)
			if err != nil {
				dump("error: ioutil.ReadAll")
				content = []byte("")
			}
			s.successRequests++

			//redis_client.HSet(s.StartUrl, url, string(content))

			ch <- &HttpResponse{url, resp, string(content), err}
		}(url)
	}

	for {
		select {
		case r := <-ch:

			end_time := time.Now().Sub(timing[md5.Sum([]byte(r.url))])
			parsed,_ := ParseUrl(r.url)
			
			if s.Configuration.Verbose {
				fmt.Printf(" %s => %s \n", parsed.path, end_time)
			}
			
			responses = append(responses, r)

			if (len(responses) + len(erroras)) == len(urls) {
				return responses
			}
		default:
		
			if s.Configuration.Verbose {
				fmt.Printf(".")
			}
			
			if (len(responses) + len(erroras)) == len(urls) {
				return responses
			}
			time.Sleep(time.Millisecond * 100)
		}
	}

	return responses
}

func (s *Sitemap) getOne(urls []string) []string {

	results := s.AsyncHttpGets(urls)

	total_string := ""

	for _, r := range results {
		total_string = total_string + r.content
	}

	return s.FindLinks(total_string)
}
func (s *Sitemap) FindLinks(content string) []string {

	var links []string

	reg := regexp.MustCompile(`href=("|')([^"']*)`)
	matches := reg.FindAllStringSubmatch(string(content), -1)

	for _, match := range matches {

		if len(match) < 2 || len(match[2]) < 2 {
			continue
		}

		link := match[2]

		// pomijanie linków kierujących do nieinteresujących nas zasobów
		found_forbidden := containsLike([]string{"#", "<%=", ".ico", "mailto", "?", ".doc", ".pdf", ".jpg", ".png", ".jpg", ".css", ".xml", ".xls", "javascript:", ".exe", ".gif", ".zip",".go"}, link)
		if found_forbidden {
			continue
		}

		// dopisanie protokołu do linków absolutnych bez protokołu
		first_two_letters := link[:2]
		if strings.Index(link, "http") != 0 && first_two_letters == "//" {
			link = "http:" + link
		}
		// dopisanie domeny do linków relatywnych
		first_letter := link[:1]
		if strings.Index(link, "http") != 0 && first_letter == "/" {
			links = append(links, clearUrl(s.StartUrl+link[1:len(link)]))
			continue
		}

		if strings.Index(link, "http") != 0 && strings.Index(link, "/") == -1 {
			links = append(links, clearUrl(s.StartUrl+link))
			continue
		}

		//abcd := config.protocol + "://" + config.domain + "/"
		//if strings.Index(link, abcd) == 0 {
		//	links = append(links, clearUrl(link))
		//}

		if strings.Index(link, s.StartUrl) == 0 {
			links = append(links, clearUrl(link))
		}

		if inArray(link, s.Other) == false {
			s.Other = append(s.Other, link)
		}

		// pomijamy linki z obcej domeny
		//if config.only_one_domain == true {
		//	if is_domain == false || strings.Contains(link, domain) == false {
		//		continue
		//	}
		//}

		//links = append(links, clearUrl(link))
	}
	return links

}

type summary struct {
	
	TotalTime			time.Duration
	TotalRequests 		uint32
	SuccessRequests 	uint32
	ErrorRequests 		uint32
	
	AverageTimePerRequest time.Duration
}


func (s *Sitemap) setSummary() {
	
	duration := time.Now().UnixNano() - s.startTime.UnixNano()
	
	s.summary.TotalTime =  time.Now().Sub(s.startTime)
	
	s.summary.TotalRequests = s.totalRequests
	s.summary.SuccessRequests = s.successRequests
	s.summary.ErrorRequests = s.errorRequests

	if s.summary.TotalRequests > 0 {
		durationInNano := time.Unix(0,(duration / int64(s.totalRequests)))
		s.summary.AverageTimePerRequest = durationInNano.Sub(time.Unix(0,0))
	}
}

func (s *Sitemap) GetSummary() summary {
	
	return s.summary
}

func errorLog( line string) {

	filepath := "error_log"

	if _, err := os.Stat(filepath); err != nil {
		if os.IsNotExist(err) {
			ioutil.WriteFile(filepath, []byte(""), 0777)
		}
	}

	f, err := os.OpenFile(filepath, os.O_APPEND|os.O_WRONLY, 0600)
	if err != nil {
		panic(err)
	}

	defer f.Close()

	if _, err = f.WriteString(line + "\n"); err != nil {
		panic(err)
	}
}