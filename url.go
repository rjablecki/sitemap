package sitemap

import (
	"errors"
	"strings"
)

type Url struct {
	scheme 		string
	host		string
	subdomain 	string
	domain		string
	path		string
	query		string
	hash		string
}

func ParseUrl (url string) (*Url, error) {
	
	a := new(Url)
	
	if len(url) < 1 {
		return a, errors.New("Url is to short to be valid")
	}
	if strings.Index(url, "//") == 0 {
		url = "http:" + url
	}
	
	if strings.Index(url, "://") > -1 {
		a.scheme = url[0:strings.Index(url, "://")]
		url = url[strings.Index(url, "://")+3:]
	}
	
	if strings.Index(url, "/") > -1 {
		b := strings.SplitN(url, "/", 2)
		a.host = b[0]
	}
	
	if strings.Index(url, "#") > -1 {
		d := strings.Split(url, "#")
		a.hash = d[1]
		url = d[0]
	}
	
	if strings.Index(url, "?") > -1 {
		c := strings.SplitN(url, "?", 2)
		a.query = c[1]
		url = c[0]
	}
	if strings.Index(url, "/") == -1 || url == "/"{
		a.path = "/"
	} else {
		p := strings.Split(url, "/")
		for i, t := range p {
			if i > 0 {
				a.path = a.path + "/" + t
			}
		}
	}
	a.parseHost()
	return a, nil
}

func (u *Url) parseHost() {
	h := u.host
	
	var subdomains []string
	var domain []string
	var is_second_domain bool
	
	if len(h) == 0 || strings.Index(h, ".") == -1{
		return
	}
	parts := strings.Split(h, ".")
	l := len(parts)
	
	long_domains := []string{"net", "wrocław", "com"}
	if inArray(parts[l-2], long_domains){
		is_second_domain = true
	} else {
		is_second_domain = false
	}
	
	for i, part := range parts {
		if i >= l - 2 {
			domain = append(domain, part)
			continue;
		}
		if i == l - 3 {
			if is_second_domain {
				domain = append(domain, part)
				continue;
			} 
		}
		subdomains = append(subdomains, part)
	}
	u.subdomain = strings.Join(subdomains, ".")
	u.domain = strings.Join(domain, ".")
}