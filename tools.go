package sitemap

import (
	"fmt"
	"strings"
	"regexp"
)

// dump variable like PHP var_dump
func dump(param interface{}) {
	fmt.Printf("%T", param)
	fmt.Printf(" (%+v)\n", param)
}
// 
func inArray(item string, slice []string) bool {
	set := make(map[string]struct{}, len(slice))
	for _, s := range slice {
		set[s] = struct{}{}
	}
	_, ok := set[item]
	return ok
}
func containsLike(slice []string, s string) bool {

	for _, el := range slice {
		if strings.Contains(s, el) {
			return true
		}
		upper := strings.ToUpper(el)
		if strings.Contains(s, upper) {
			return true
		}
	}
	return false
}

func getSiteFromUrl(url string) (string, bool) {
	reg := regexp.MustCompile(`^(https?:\/\/[^\/]*)\/`)
	matches := reg.FindAllStringSubmatch(url, -1)

	if len(matches) < 1 {
		return "", false
	}

	return matches[0][1], true
}

func getDomainFromUrl(url string) (string, bool) {
	reg := regexp.MustCompile(`^https?:\/\/([^\/]*)`)
	matches := reg.FindAllStringSubmatch(url, -1)

	if len(matches) < 1 {
		return "", false
	}
	parts := strings.Split(matches[0][1], ".")
	l := len(parts)

	return parts[l-2] + "." + parts[l-1], true
}